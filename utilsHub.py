import cv2
from sklearn.svm import LinearSVC
import numpy as np
import pickle
from charLocalization import findChar, imcrop
import time
from statistics import mode, StatisticsError
from collections import Counter
from predict import Province
from mysql.connector.errors import OperationalError
import mysql.connector
from urllib.request import Request
from skimage.feature import hog
import json
import base64
def randomString(length=16):
    token = ''
    for i in range(length):
        token += random.choice('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'+'1234567890'*2)
    return token
def imcrop2(img, rectArray):
    return img.copy()[rectArray[1]:rectArray[3], rectArray[0]:rectArray[2]]

def licenseplateOCR(image, skSVMclassifier, getRectangles=False):
    """"
    This function will return 2 row of text those are license number and province
    """
    
    im = cv2.resize(image, (400, 200))
    if image.size < 400*200:
        blur_px = int(((400*200)/image.size))
        if blur_px >= 1:
            try:
                im = cv2.addWeighted(im, 1.5, cv2.GaussianBlur(im, (blur_px,blur_px), 10.0), -0.5, 0, im)
            except:
                pass
        #im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    #im = cv2.equalizeHist(im)
    #cv2.imshow("Sharpen",im)
    rects, show = findChar(im, True, True)
    text = ['','']
    rects = sorted(rects,key=lambda l:l[0])
    for i in range(len(rects)):
        cimg = imcrop(im, rects[i])
        cimg = cv2.resize(cimg, (28, 28))
        cimg = cv2.cvtColor(cimg, cv2.COLOR_BGR2GRAY)
        cimg = cv2.equalizeHist(cimg)
        cimg = hog(cimg, orientations=8, pixels_per_cell=(4, 4),
                    cells_per_block=(1, 1), visualize=False, multichannel=False, block_norm='L1')
        c = skSVMclassifier.predict(np.array(cimg).reshape(1, -1))
        if c[0] == '-':
            pass
        elif rects[i][1] > im.shape[0]*0.5:
            if rects != 0:
                if rects[i-1][0]+(rects[i-1][2]*2) >= rects[i][0]:
                    text[1] += ' '
            text[1] += c[0]
        else:
            text[0] += c[0]
        #cv2.putText(im,c[0],(rects[i][0],rects[i][1]), font, 1,(0,255,255),2,cv2.LINE_AA)
    if getRectangles:
        return (text[0], text[1], im, rects)
    else:
        return (text[0], text[1])

def insertLpToDict(d, lp_tup):
    if type(lp_tup) is not dict:
        pass
    try:
        d['num'].append(lp_tup['num'])
        d['province'].append(lp_tup['province'])
    except:
        d = {'num':[lp_tup['num']], 'province':[lp_tup['province']]}
    return d
def jsonDataSend(dict_of_data, url):
    params = json.dumps(dict_of_data).encode('utf8')
    req = Request(url, data=params, headers={'content-type': 'application/json'})
    response = urllib.request.urlopen(req)
    return response.status

def mat2JpgStr(image):
    retval, buffer = cv2.imencode('.jpg', image)
    return base64.b64encode(buffer)

def JpgStr2mat(jpg_as_text):
    jpg_original = base64.b64decode(jpg_as_text)
    jpg_as_np = np.frombuffer(jpg_original, dtype=np.uint8)
    return cv2.imdecode(jpg_as_np, flags=1)
    
def record(stream_id, licenseplates , cropped, frame, cnx, configs, last_lp='', mode='normal'):
    timeStampRaw = time.time()
    timeStamp = str(int(timeStampRaw*1000)-1539000000000)
    if len(licenseplates['num']) != 0: 
        #try:
        if mode == 'force':
            try:
                lpResult = mode(licenseplates['num'])
                pred_province_id = mode(licenseplates['province'])
            except:
                lpResult = licenseplates['num'][0]
                pred_province_id = licenseplates['province'][0]
        else:    
            lpResultCounter = Counter(licenseplates['num'])
            if len(licenseplates['province']) == 0:
                licenseplates['province'].append('')
            pred_province_id = Counter(list(filter(lambda i : type(i) is int, licenseplates['province']))).most_common()[0][0]
            #if len(lpResultCounter.values()) == len(set(lpResultCounter.values())) and lpResultCounter.most_common(1)[0][1] >= 3:
            if lpResultCounter.most_common()[0][1] >= 3:
                lpResult = lpResultCounter.most_common()[0][0] 
                #print(lpResultCounter)
            else:
                return last_lp, False
        if lpResult != last_lp and len(lpResult) > 2 and len(lpResult) <= 7 and len(lpResult)-sum(map(str.isdigit, lpResult)) <= 3:
            try:
                try:
                    http_resp = jsonDataSend({"time":timeStampRaw, "lp_number":lpResult, "cam_id":stream_id, "province_id":pred_province_id, "full_image":mat2JpgStr(frame), "cropped":mat2JpgStr(cropped)}, configs['target_url'])
                    if http_resp != 200:
                        print("Server not respond! Data not send:", http_resp)
                except Exception as e:
                    print("Data sending error!:",e)       
                try: 
                    cursor = cnx.cursor()
                except OperationalError:
                    cnx.close()
                    cnx = mysql.connector.connect(**configs['mysql'])
                    cursor = cnx.cursor()
                cursor.execute("""INSERT INTO licenseplate 
                            (lp_id, lp_number, lp_datetime, lp_img_path, LP_IMG_FILENAME, Camera_CAM_ID, Province_PROVINCE_ID) 
                            VALUES (%s, %s, %s, %s, %s, %s, %s)""", (timeStamp, lpResult, time.strftime('%Y-%m-%d %H:%M:%S'), configs['appconfig']['img_path'], (str(stream_id)+'_'+str(timeStamp)+".jpg"), stream_id, pred_province_id))
                
                cnx.commit()
                cursor.close()
                #record Province will not work now
                #print("Recorded" , lpResult, province, end='')
                print("Recorded" , lpResult, pred_province_id, end='')
                if type(cropped) is not bool:
                    cv2.imwrite(configs['appconfig']['img_path']+"\\"+str(stream_id)+'_'+str(timeStamp)+".jpg", \
                                cropped)
                    cv2.imwrite(configs['appconfig']['img_path']+"\\"+str(stream_id)+'_'+str(timeStamp)+"f.jpg", \
                                frame)
                    print("- Image file:", configs['appconfig']['img_path']+"\\"+str(stream_id)+'_'+str(timeStamp)+".jpg")
                else:
                    print("- Image not recorded")
                return lpResult, True
            except Exception as e:
                print("Record error:", e)
                #print("Not Recorded" , lpResult, province)
                print("Not Recorded" , lpResult)
                return last_lp, False
             
        #except Exception as e:
        #     if type(e) is StatisticsError:
        #         #print('-')
        #         #r = -1
        #         pass
        #     else:
        #         print(e)
    return last_lp, False

""" def licenseplateDataProcess(frame_data, clf):
    text = [' ',' ']
    predicted_province=''
    boxes = frame_data['boxes']
    scores = frame_data['scores']
    classes = frame_data['classes']
    num = frame_data['num']
    frame = frame_data['frame']
    im_height = frame_data['frame'].shape[0]
    im_width = frame_data['frame'].shape[1] 
    for i in range(len(scores[0])):
        if scores[0][i] > 0.8:
            #lpList.append(boxes[0][i])
            ## DRAW result
            cropped = frame[int(boxes[0][i][0]*im_height):int(boxes[0][i][2]*im_height),int(boxes[0][i][1]*im_width):int(boxes[0][i][3]*im_width)]
            if (cropped.shape[0] / cropped.shape[1]) < 0.8:
                cv2.rectangle(frame, (int(boxes[0][i][1]*im_width), int(boxes[0][i][0]*im_height)), (int(boxes[0][i][3]*im_width), int(boxes[0][i][2]*im_height)), (0,255,0), 1)
                
                text = licenseplateOCR(cropped,clf)
                #licenseplates['num'].append(text[0])
                predicted_province = Province(text[1])
                #licenseplates['province'].append(predicted_province)
                cv2.imshow('Object detector '+str(frame_data['stream_id']), frame)
                if len(text[0]) > 2 and len(text[0]) <= 7 and len(text[0])-sum(map(str.isdigit, text[0])) <= 3:
                    #print(text[0], predicted_province)
                    return {"stream_id":frame_data['stream_id'], "num":text[0], "province":predicted_province.id(), "cropped":cropped, "frame":frame}
    cv2.imshow('Object detector '+str(frame_data['stream_id']), frame)
    return {"stream_id":frame_data['stream_id'], "num":'', "province":'', "cropped":None, "frame":frame} """

def licenseplateDataProcess(frame_data, clf):
    text = [' ',' ']
    predicted_province=''
    x = frame_data['x']
    y = frame_data['y']
    h = frame_data['h']
    w = frame_data['w']
    frame = frame_data['frame']
    im_height = frame_data['frame'].shape[0]
    im_width = frame_data['frame'].shape[1] 
    cropped = frame[y:y+h,x:x+w]
    text = licenseplateOCR(cropped,clf)
    predicted_province = Province(text[1])

    if len(text[0]) > 2 and len(text[0]) <= 7 and len(text[0])-sum(map(str.isdigit, text[0])) <= 3:
        return {"stream_id":frame_data['stream_id'], "num":text[0], "province":predicted_province.id(), "cropped":cropped, "frame":frame}
    return {"stream_id":frame_data['stream_id'], "num":'', "province":'', "cropped":None, "frame":frame}