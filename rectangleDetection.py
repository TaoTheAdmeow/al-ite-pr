import cv2
import numpy as np
from threading import Thread
import time
from charLocalization import findChar, imcrop
import pickle 
def union(a,b):
  x = min(a[0], b[0])
  y = min(a[1], b[1])
  w = max(a[0]+a[2], b[0]+b[2]) - x
  h = max(a[1]+a[3], b[1]+b[3]) - y
  return (x, y, w, h)

def intersection(a,b):
  x = max(a[0], b[0])
  y = max(a[1], b[1])
  w = min(a[0]+a[2], b[0]+b[2]) - x
  h = min(a[1]+a[3], b[1]+b[3]) - y
  if w<0 or h<0: return ()
  return (x, y, w, h)

def combine_boxes(boxes):
    noIntersectLoop = False
    noIntersectMain = False
    posIndex = 0
    # keep looping until we have completed a full pass over each rectangle
    # and checked it does not overlap with any other rectangle
    while noIntersectMain == False:
        noIntersectMain = True
        posIndex = 0
         # start with the first rectangle in the list, once the first 
         # rectangle has been unioned with every other rectangle,
         # repeat for the second until done
        while posIndex < len(boxes):
            noIntersectLoop = False
            while noIntersectLoop == False and len(boxes) > 1:
                a = boxes[posIndex]
                listBoxes = np.delete(boxes, posIndex, 0)
                index = 0
                for b in listBoxes:
                    #if there is an intersection, the boxes overlap
                    if intersection(a, b): 
                        newBox = union(a,b)
                        listBoxes[index] = newBox
                        boxes = listBoxes
                        noIntersectLoop = False
                        noIntersectMain = False
                        index = index + 1
                        break
                    noIntersectLoop = True
                    index = index + 1
            posIndex = posIndex + 1
    return boxes.astype("int")
FRAMES = {1:np.zeros((480,640,3), dtype=np.uint8)}
# load the image, convert it to grayscale, and blur it
def startVideo(uri , stream_id):
    global FRAMES
    print("Connecting to", uri)
    errRound = 0
    try:
        video = cv2.VideoCapture(uri)
        print("Connected to", uri)
        while(1):
            if video.isOpened() and FRAMES[stream_id] is not None:
                ret, frame = video.read()
                FRAMES[stream_id] = frame
            else:
                frame = np.zeros((480,640,3), dtype=np.uint8)
                frame = cv2.putText(frame, "Can't recieve video!", (100,64), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
                FRAMES[stream_id] = frame
                video.release()
                print("Camera disconnected! Reconnecting - Cooldown 10 sec... attempt:", errRound)
                time.sleep(10)
                try:
                    video = cv2.VideoCapture(uri)
                    errRound = 0
                except:
                    pass
                    errRound += 1
    except Exception as e:
        video.release()
        print(e)

VT= Thread(target = startVideo, args=(0, 1) )
VT.start()
lpclf = pickle.load(open('LPmodel3.pkl','rb'))
while(1):
    confidences = []
    img = FRAMES[1].copy()
    raw = FRAMES[1].copy()
    img = cv2.resize(img, (400,400))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = cv2.blur(img,(1,1))
    #img = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
    #       cv2.THRESH_BINARY,11,2)
    #ret3,img = cv2.threshold(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    img = cv2.Canny(img,80,160)
    #img = cv2.bitwise_not(img)
    im_width, im_height = img.shape
    im_size = img.size
    im2, contours, hierarchy = cv2.findContours(img,cv2.RETR_TREE,cv2.cv2.CHAIN_APPROX_SIMPLE)
    rects = [cv2.boundingRect(ctr) for ctr in contours]
    im2 = cv2.cvtColor(im2, cv2.COLOR_GRAY2BGR)
    charsLocation = []
    for rect in rects:
        # Draw the rectangles
        if rect[2]/rect[3] < 2 and rect[2]/rect[3] > 0.5 and rect[2] * rect[3] > im_size * 0.002 and rect[2] * rect[3] < im_size * 0.8:
            charsLocation.append(rect)
   # try:
    #    charsLocation = combine_boxes(charsLocation)
    #except:
    #    pass
    for rect in charsLocation:
        cropped = cv2.resize(imcrop(raw, rect), (28,28))
        cropped = cv2.cvtColor(cropped, cv2.COLOR_BGR2GRAY)
        cropped = cv2.equalizeHist(cropped).reshape(1,-1)
        confidences.append(lpclf.predict_proba(cropped)[0][1])
    #sort charsLocation by confidence
    sortedCharLocation = [x for _,x in sorted(zip(confidences,charsLocation))]
    for conf, rect in zip(confidences, sortedCharLocation):
        if conf > 0:
            cv2.rectangle(im2, (rect[0], rect[1]), (rect[0] + rect[2], rect[1] + rect[3]), (0, 255, 0), 1)
    cv2.imshow('w', im2)
    if cv2.waitKey(1) == ord('q'):
        cv2.destroyAllWindows()
        break
        