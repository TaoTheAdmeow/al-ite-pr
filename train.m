
imDir = fullfile('E:','dataset','raw','Images',...
    'img','positive');
addpath(imDir);
negativeFolder = fullfile('E:','dataset','negative');
negativeImages = imageDatastore(negativeFolder);
trainCascadeObjectDetector('SignDetector5.xml',positive, ...
    negativeFolder,'FalseAlarmRate',0.45,'NumCascadeStages',13,'TruePositiveRate',0.99,'FeatureType','LBP');