import cv2
import numpy as np
import os
import time
import itertools

def union(a,b):
  x = min(a[0], b[0])
  y = min(a[1], b[1])
  w = max(a[0]+a[2], b[0]+b[2]) - x
  h = max(a[1]+a[3], b[1]+b[3]) - y
  return (x, y, w, h)

def intersection(a,b):
  x = max(a[0], b[0])
  y = max(a[1], b[1])
  w = min(a[0]+a[2], b[0]+b[2]) - x
  h = min(a[1]+a[3], b[1]+b[3]) - y
  if w<0 or h<0: return ()
  return (x, y, w, h)

def combine_boxes(boxes):
    noIntersectLoop = False
    noIntersectMain = False
    posIndex = 0
    # keep looping until we have completed a full pass over each rectangle
    # and checked it does not overlap with any other rectangle
    while noIntersectMain == False:
        noIntersectMain = True
        posIndex = 0
         # start with the first rectangle in the list, once the first 
         # rectangle has been unioned with every other rectangle,
         # repeat for the second until done
        while posIndex < len(boxes):
            noIntersectLoop = False
            while noIntersectLoop == False and len(boxes) > 1:
                a = boxes[posIndex]
                listBoxes = np.delete(boxes, posIndex, 0)
                index = 0
                for b in listBoxes:
                    #if there is an intersection, the boxes overlap
                    if intersection(a, b): 
                        newBox = union(a,b)
                        listBoxes[index] = newBox
                        boxes = listBoxes
                        noIntersectLoop = False
                        noIntersectMain = False
                        index = index + 1
                        break
                    noIntersectLoop = True
                    index = index + 1
            posIndex = posIndex + 1
    return boxes.astype("int")

def findChar(img, show=False, returnResult=False):
    """
    THIS WILL RETURN RECTANGLE IN LIST
    """
    try:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = cv2.blur(img, (3,3))
    except:
        pass
    #ret3,img = cv2.threshold(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    img = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
            cv2.THRESH_BINARY,11,2)
    img = cv2.bitwise_not(img)
    im_width, im_height = img.shape
    im_size = img.size
    im2, contours, hierarchy = cv2.findContours(img,cv2.RETR_TREE,cv2.cv2.CHAIN_APPROX_NONE)
    rects = [cv2.boundingRect(ctr) for ctr in contours]
    #rects = sorted(rects,key=lambda l:l[0])
    #rects, weights=cv2.groupRectangles(rects,1)
    #print (im_size)
    im2 = cv2.cvtColor(im2, cv2.COLOR_GRAY2BGR)
    charsLocation = []
    for rect in rects:
        # Draw the rectangles
        if rect[2]/rect[3] < 2.2 and rect[2]/rect[3] > 0.25 and rect[2]*rect[3] > img.size*0.002 and rect[2]*rect[3] < img.size*0.1:
            charsLocation.append(rect)
            #if show or returnResult:
            #   cv2.rectangle(im2, (rect[0], rect[1]), (rect[0] + rect[2],\
            #                                    rect[1] + rect[3]), (0, 255, 0), 1)
    try:
        charsLocation = combine_boxes(charsLocation)
    except:
        charsLocation = charsLocation 
    if show or returnResult:
        for rect in charsLocation:
            cv2.rectangle(im2, (rect[0], rect[1]), (rect[0] + rect[2],\
                                                rect[1] + rect[3]), (0, 255, 0), 1)
        if show:
            cv2.imshow('w', im2)
    if returnResult:
        return charsLocation, im2
    else:
        return charsLocation

def imcrop(img, rectArray):
    return img[rectArray[1]:rectArray[1]+rectArray[3], rectArray[0]:rectArray[0]+rectArray[2]]

def writeJPG():
    from sklearn.svm import LinearSVC
    import pickle
    clf = pickle.load(open('OCRModel2.pkl','rb'))
    letters = '0123456789-กขฃคฅฆงจฉชซฌญฎฏฐฑฒณดตถทธนบปผฝพฟภมยรลวศษสหฬอฮ'
    for f in os.listdir("H:\\dataset\\SignOnly"):
        savename = ''
        imgC = cv2.imread('H:\\dataset\\SignOnly\\'+f)
        img = cv2.cvtColor(imgC, cv2.COLOR_BGR2GRAY)
        rects, result = findChar(img, False, True)
        cv2.imwrite("H:\\dataset\\RESULT\\"+f, result)
        for rect in rects:
            savename = ''
            #savename = f+str(rect[0])+"_"+str(rect[1])+".jpg"
            try:
                cropped_im = cv2.resize(imcrop(imgC, rect), (12, 20))
                #cimg = cv2.equalizeHist(cropped_im)
                cimg = np.array(cropped_im.reshape((1, -1)))
                c = clf.predict(cimg)
                if c[0] == '-':
                    savename = 'D_'+(str(int(time.time()*100))[6:])
                else:
                    savename = str(letters.index(c[0]))+'_'+(str(int(time.time()*100))[6:])
                cropped_im = cv2.cvtColor(cropped_im, cv2.COLOR_BGR2GRAY)
                cv2.imwrite("H:\\dataset\\CHAR\\"+savename+".jpg", cropped_im)
                print(savename+" saved.")
            except Exception as e:
                
                print(savename+" not saved. Error:", e)

if __name__ == '__main__':
    writeJPG()
