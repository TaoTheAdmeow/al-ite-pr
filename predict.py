import json
import re
import operator

class Province:
    json_data = open("province.json", encoding="utf8").read()
    data = json.loads(json_data)

    result = []
    name = ""
    index = 0

    def predict(self):
        return self.name

    def id(self):
        return self.index

    def getAll(self):
        return self.result
    
    def GetPredictId(text):
        for i in Province.data:
            if text == i["PROVINCE_NAME"]:
                return i["PROVINCE_ID"]

    def predictProvinceIntersection(text):
        sector_i = 0
        result = dict()

        for n, i in enumerate(Province.data):
            province = Province.data[n]["PROVINCE_NAME"]
            if province not in result:
                result[province] = 0
            if province[0] == text[0]:
                result[province] += 1
            index = 0
            for j in text:
                for nn, k in enumerate(province[index:]):
                    if j == province:
                        result[province] += 1
                        index = nn
                        
        return sorted(result.items(), key=operator.itemgetter(1), reverse=True)[0][0]

    def PredictProvinceRegEx(text):
        text.replace(' ', '')
        regex = ""
        for i in text:
            regex += i + "+.*"
        p = re.compile(regex, re.IGNORECASE)
        result = [a["PROVINCE_NAME"] for a in Province.data if p.match(a["PROVINCE_NAME"]) != None]

        if result == []:
            return Province.predictProvinceIntersection(text)  
        
        return result[0]

    def PredictProvince(text):
        return Province.PredictProvinceRegEx(text)

    def PredictProvinceWithId(text):
        province = Province.PredictProvinceRegEx(text)
        return [Province.GetPredictId(province), province]

    def __init__(self, sampleText):
        #json_data = open("province.json", encoding="utf8").read()
        #data = json.loads(json_data)
        self.result = Province.PredictProvinceWithId(sampleText)
        self.index = self.result[0]
        self.name = self.result[1]

if __name__ == "__main__":
    while True:
        sampleText = input()
        province = Province(sampleText)
        print(province.predict(), province.id(), province.getAll())
