import numpy as np
import cv2
from threading import Thread
import time
import json
import mysql.connector

from onvif_utils import getONVIFuri
#from OCR2 import licenseplateOCR
from utilsHub import *
cv2.ocl.setUseOpenCL(True)
IS_RUN = True
with open('appSettings.json', 'r') as infile:  
    configs = json.load(infile)
try:
    cnx = mysql.connector.connect(**configs['mysql'])
except Exception as e:
    print("error: Can't connect to SQL server! Application aborting!:", e)
    exit()
try:
    cursor = cnx.cursor()
    cursor.execute("SELECT CAM_ID, CAM_URI, CAM_PORT, CAM_TYPE, CAM_USERNAME, CAM_PASSWORD FROM camera WHERE CAM_ACTIVE = 1;")
    result = cursor.fetchall()
    if len(result) == 0:
        print("Camera not found! Add or activate camera to begin!")
        exit()
    else:
        print("Camera found!")
except Exception as e:
    print("Error table not found!")

FRAMES = {}
VIDEO_NAME = {}
#video = {}
clf = pickle.load(open('OCRModelHOG.pkl','rb'))
MODE = 'online'

for row in result:
    if row[3] == "ONVIF" or row[3] == "ONVI":
        try:
            VIDEO_NAME[row[0]] = getONVIFuri(row[1], row[4], row[5], row[2])
            print(VIDEO_NAME[row[0]], 'added')
        except Exception as e:
            print("ONVIF Connection fail!:", e)
    elif row[3] == 'URI':
        try:
            if len(row[4])-row[4].count(' ')+len(row[5])-row[5].count(' ') != 0:
                VIDEO_NAME[row[0]] = (row[4]+':'+row[5]+'@'+row[1])
            else:
                VIDEO_NAME[row[0]] = (row[1])
        except:
            VIDEO_NAME[row[0]] = (row[1])
            
    FRAMES[row[0]] = m = np.zeros((200,200,3), dtype=np.uint8) #initial for frame storage

def startVideo(uri , stream_id):
    global FRAMES
    global IS_RUN
    print("Connecting to", uri)
    errRound = 0
    try:
        video = cv2.VideoCapture(uri)
        video.set(3,1280)
        video.set(4,720)
        print("Connected to", uri)
        while(IS_RUN):
            if video.isOpened() and FRAMES[stream_id] is not None:
                ret, FRAMES[stream_id] = video.read()
            else:
                frame = np.zeros((480,640,3), dtype=np.uint8)
                frame = cv2.putText(frame, "Can't recieve video!", (100,64), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
                FRAMES[stream_id] = frame
                video.release()
                print("Camera disconnected! Reconnecting - Cooldown 5 sec... attempt:")
                time.sleep(5)
                try:
                    video = cv2.VideoCapture(uri)
                except:
                    pass
    except Exception as e:
        video.release()
        print("Video thread error: ",e)


def confidenceFilter(l):
        l = [int(item[0]) for item in l ]
        l = list(filter(lambda elm: isinstance(elm, int), l))
        max_val = max(l)
        max_idx = l.index(max_val)
        return max_idx, max_val


def startDetection(stream_id, cnx, configs):
    global FRAMES
    global IS_RUN
    cascade_detector = cv2.CascadeClassifier('SignDetector5.xml')
    licenseplates = {'num':[], 'province' :[]}
    last_lp = ''
    time_start = time.time()
    no_car_duration_start = 0
    detected_streak = 0
    max_streak = 0
    gray_im_base = []
    while(1):
        start_time = time.time()
        lpList = list()
        try:
            fullim = FRAMES[stream_id].copy()
        except:
            fullim = np.zeros((960,540,3), dtype=np.uint8)
        im_height = fullim.shape[0]
        im_width = fullim.shape[1]
        img = cv2.resize(fullim, (960, 540))
        #gray_im_small = cv2.resize(fullim, (480, 270))
        signs, conf = cascade_detector.detectMultiScale2(cv2.UMat.get(cv2.cvtColor(cv2.UMat(img), cv2.COLOR_BGR2GRAY)), 1.1, 18, minSize=(32,16), maxSize=(400,200))
        #print(conf)
        if len(conf) > 0:
            idx, a_conf = confidenceFilter(conf)
            (x,y,w,h) = signs[idx]
            cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
            y,h = map(lambda i: int(i/540.0*im_height), (y,h))
            x,w = map(lambda i: int(i/960.0*im_width), (x,w))
            #cv2.putText(img,  str(c), (x,y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255), 1, cv2.LINE_AA)
            frame_data = {'frame':fullim.copy(),
            'x':x, 'y':y, 'w':w,'h':h, 'stream_id':stream_id}
            processed_lp_datas = licenseplateDataProcess(frame_data, clf)
            if len(processed_lp_datas['num']) > 0:
                licenseplates['num'].append(processed_lp_datas['num'])
                licenseplates['province'].append(processed_lp_datas['province'])
                first_lp_image, first_lp_image_cropped = processed_lp_datas['frame'].copy(), processed_lp_datas['cropped'].copy()
                detected_streak += 1
                no_plate_time_start = time.time()
            else:
                if detected_streak >= 3:
                    max_streak = detected_streak
                detected_streak = 0

            if licenseplates and time.time() - time_start >= 0.5:
                try:
                    cv2.imshow('cropped', processed_lp_datas["cropped"].copy())
                except:
                    pass
                last_lp, is_record = record(stream_id, licenseplates, processed_lp_datas["cropped"], frame_data['frame'], cnx, configs, last_lp)
                if is_record:
                    time_start = time.time()
                    licenseplates = {'num':[], 'province' :[]}
            elif time.time() - time_start >= 3: #clear false-positive-noise licenseplate every 3 sec.
                time_start = time.time()
                licenseplates = {'num':[], 'province' :[]}
        try:
            fps = 10000.0/((time.time() - start_time)*10000)
        except:
            fps = '--'
        cv2.putText(img,"fps:"+str(fps),(20,20), cv2.FONT_HERSHEY_SIMPLEX, 0.5,(255,255,255),2,cv2.LINE_AA)
        cv2.imshow('Cascade'+str(stream_id),img)
        if cv2.waitKey(1) == ord('q'):
            cv2.destroyAllWindows()
            IS_RUN = False
            break
if __name__ == '__main__':
    FRAMEQ = {}
    #FRAMES = {}
    T = {}
    VT = {}
    #cascade_detector = cv2.CascadeClassifier('SignDetector2.xml')
    #VTt= Thread(target = startVideo, args=(0, 0) )
    #VTt.start()
    #Tt = Thread(target =  startDetection, args = (0, cnx, configs))
    #Tt.start()
    for row in result:
        try:
            print("Starting Detector id:", row[0])
            
            #FRAMEQ[row[0]] = queue.Queue(maxsize=1)
            #FRAMEQ[row[0]].put_nowait(np.zeros((480,640,3), dtype=np.uint8))
            VT[row[0]] = Thread(target = startVideo, args=(VIDEO_NAME[row[0]], row[0]) )
            T[row[0]] = Thread(target =  startDetection, args = (row[0], cnx, configs))
            VT[row[0]].start()
            T[row[0]].start()
        except Exception as e:
            print("Error: ", e, "Source ID:" , row[0])


