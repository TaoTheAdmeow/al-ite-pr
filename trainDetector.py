import cv2
import numpy as np
from threading import Thread
import time
import os
from sklearn.svm import LinearSVC, SVC
import pickle
trainData = []
labels = []
PATH_POSITIVE = 'E:\\dataset\\raw\\Images\\img\\positive'
PATH_NEGATIVE = 'E:\\dataset\\raw\\Images\\img\\negative'
for path, label  in [(PATH_POSITIVE, 1),( PATH_NEGATIVE, 0)] :
    for f in os.listdir(path):
        try:
            print(f, 'read.')
            img = cv2.imread(path+"\\"+f)
            
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            img = cv2.resize(img, (28,28))
            img = cv2.equalizeHist(img)
            reshaped_img = img.reshape(-1)
            labels.append(label)
            trainData.append(reshaped_img)
        except Exception as e:
            print(f, 'is not image. or Error:', e)
print('Training...')
clf = SVC(probability=True, max_iter=1000000)
#clf = LinearSVC()
clf.fit(trainData, labels)
print('Trained')
pickle.dump(clf, open('LPmodel3.pkl','wb'))

    #cv2.imshow('w', raw)
    #if cv2.waitKey(1) == ord('q'):
    #    cv2.destroyAllWindows()
    #    break
        