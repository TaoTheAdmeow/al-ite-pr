import cv2
video = cv2.VideoCapture(0)
last_frame = None
backSub = cv2.createBackgroundSubtractorKNN()
while(video.isOpened()):
    
    ret, frame = video.read()
    frame = cv2.UMat(frame)
    #print(frame)
    #if last_frame == None:
        #last_frame = frame.copy()
    #frame = cv2.medianBlur(frame ,8)
    fgMask = backSub.apply(frame)
   # print(fgMask)
    blur = cv2.blur(fgMask ,(3,3))
    ret3,img = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    cv2.imshow('FG Mask', img)
    keyboard = cv2.waitKey(1)
    if keyboard == 'q' or keyboard == 27:
        cv2.destroyAllWindows()
        break
    
